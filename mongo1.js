db.fruits.aggregate([
    { $match: {onSale: true} },
    { $group: {_id: "$supplier_id", avg_stock: {$avg: "$stock"}}}
    ]);