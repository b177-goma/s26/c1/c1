db.fruits.aggregate([
    { $match: { onSale: true} },
    { $match: { origin: "Philippines"}},
    { $group: {_id: "Philippines_id", max_price: { $max: "$price" } } }
]);
